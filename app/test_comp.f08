! Basically calls each script to make sure everything is printed out correctly.
program call_comp
  use compression
  implicit none
  character(30) :: filename, tar_name, zip_name
  character(1) :: arg

  tar_name = "tarrin.tar"
  zip_name = "Be_zippin.zip"
  filename = "this_working"
  arg = "f"
  ! first with arguments then without
  call gzip(filename)
  call gzip(filename, arg)
  
  call gunzip(filename)

  call zip(zip_name, filename)
  call zip(zip_name, filename, arg)

  call bzip2(filename)
  call bzip2(filename, arg)

  call xz(filename)
  call xz(filename,arg)

  call rar(filename)
  call rar(filename, arg)

  call unrar(filename)
  call unrar(filename, arg)

  call Sevenzip(filename)
  call Sevenzip(filename, arg)

  call tar(tar_name, filename)
  call tar(tar_name, filename, arg)
end program call_comp
